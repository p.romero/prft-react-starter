import { createMuiTheme } from "@material-ui/core/styles";
import { palette } from './palette'

const themeName = "Perficient";

export default createMuiTheme({
    palette,
    themeName,
    typography: {
        useNextVariants: true
    },
    root: {
        flexGrow: 1
    },
    overrides: {
        MuiTypography: {
            root: {
                textTransform: 'capitalize'
            },
            colorError: palette.error.main,
            colorSecondary: palette.secondary.dark
        },
        MuiTooltip: {
            tooltip: {
                background: palette.secondary.dark,
                fontSize: 12
            }
        },
        MuiIconButton: {
            sizeSmall: {
                padding: 4,
                '&:hover': {
                    backgroundColor: palette.error.main
                }
            }
        }
    }
});
