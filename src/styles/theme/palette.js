import { fade } from '@material-ui/core/styles/colorManipulator';

const primaryMain = '#cc1f20';
const primaryLight = '#ff5a4a'
const primaryDark = '#930000'
const primaryUltraLight = fade(primaryMain, 0.1)
const secondaryMain = '#595959';
const secondaryLight = '#868686';
const secondaryDark = '#303030'
const secondaryUltraLight = fade(secondaryMain, 0.1)
const white = '#FFFFFF';

export const palette = {
    primary: {
        main: primaryMain,
        dark: primaryDark,
        light: primaryLight,
        ultraLight: primaryUltraLight,
        boxShadow: fade(primaryMain, 0.25)
    },
    secondary: {
        main: secondaryMain,
        dark: secondaryDark,
        light: secondaryLight,
        ultraLight: secondaryUltraLight,
    },
    divider: '#BDBDBD',
    text: {
        primary: '#212121',
        secondary: '#757575',
        subTitle: fade(white, 0.6),
        menuItem: fade(white, 0.87),
        white
    },
    error: {
        main: '#FA4444'
    },
    background: {
        body: '#FAFAFA',
        black: '#000000',
        light: '#F2F2F2',
        main: '#EBEBEB',
        white
    }
};

