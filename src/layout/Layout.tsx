import React from "react";

import Navbar from "../components/Toolbar";
import LayoutBody from "./LayoutBody";

interface Props {
  children?: any;
}

const Layout: React.SFC<Props> = (props: Props) => (
  <Navbar>
    <LayoutBody>{props.children}</LayoutBody>
  </Navbar>
);

export default Layout;
