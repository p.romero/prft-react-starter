import React, { Component } from "react";
import styled from "styled-components";

interface Props {
  className?: string;
  children?: any;
}

class LayoutBody extends Component<Props, {}> {
  render() {
    return (
      <>
        <div className={this.props.className}>{this.props.children}</div>
      </>
    )
  }
}

const StyledLayoutBody = styled(LayoutBody)`
  height: 100vh;
`;

export default StyledLayoutBody;
