export const cardGridStyles = {
    grid: {
        display: 'flex',
        flexDirection: "column" as "column",
        justifyContent: 'flex-start' as 'flex-start',
        alignItems: 'flex-start' as 'flex-start',
        margin: `0px 16px 16px`,
        padding: `0px 16px 16px`
    }
} 