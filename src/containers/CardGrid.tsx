import React from "react";
import Header from '../components/Header'
import { Grid, withStyles } from "@material-ui/core";
import { cardGridStyles } from './styles';

const CardGrid = (props: any) => {

  const { classes, title } = props;

  return (
    <div className={classes.grid}>
      <Header page="page" title={title} />
      <Grid container justify="flex-start">
        <Grid item xs={12}>
          <Grid container justify="center">
            {props.children}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(cardGridStyles)(CardGrid);
