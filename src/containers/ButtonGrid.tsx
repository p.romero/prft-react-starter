import React from "react";

import { Grid, withStyles } from "@material-ui/core";

const ButtonGrid = (props : any) => {
  const { classes } = props;

  return (
      <Grid container spacing={10} direction="column" className={classes.flex}>
        <Grid item xs={12} className={classes.flex}>
          {props.children}
        </Grid>
      </Grid>
  );
};

export default withStyles({
  flex : {
    margin : "auto",
    flex : 1
  }
})(ButtonGrid);
