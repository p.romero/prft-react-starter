import React, { useState } from "react";
import { withStyles } from '@material-ui/core/styles'
import List from "@material-ui/core/List";
import Drawer from "@material-ui/core/Drawer";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ChevronRight from '@material-ui/icons/ChevronRight';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import perficientP from '../../assets/logo/PerficientPWhite.png';
import classNames from 'classnames'
import styles from './styles'
import { TOOLBAR_ITEMS } from './ToolbarItems';
import { BottomNavigation } from "@material-ui/core";

const Toolbar = (props: any) => {

  const { classes } = props;

  const [open, setOpen] = useState(false);

  const toggleDrawer = () => setOpen(!open)

  return (
    <div className={classes.container}>
      <div className={classes.toolbar}>
        <div className={classes.linkGroup}>
          <div className={classes.logo}><IconButton size="small"><img height='35px' width='35px' src={perficientP} /></IconButton></div>
          {TOOLBAR_ITEMS.map((item, index) => {
            return (
              <div className={classes.link}>
                <Tooltip placement="right" key={index} title={item.name}>
                  <IconButton color="inherit" size="small"><item.icon /></IconButton>
                </Tooltip>
              </div>
            )
          })}
        </div>
      </div>
      {props.children}
    </div>
  );
};


export default withStyles(styles)(Toolbar);
