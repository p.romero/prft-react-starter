import ListAlt from '@material-ui/icons/ListAltOutlined';
import PersonOutlineOutlined from '@material-ui/icons/PersonOutlineOutlined';
import Work from '@material-ui/icons/WorkOutline';

const toolbarItems = {
    projects: {
        name: "Projects",
        icon: ListAlt
    },
    consultants: {
        name: "Consultants",
        icon: PersonOutlineOutlined
    },
    roles: {
        name: "Roles",
        icon: Work

    }
}

export const TOOLBAR_ITEMS = [
    toolbarItems.projects,
    toolbarItems.consultants,
    toolbarItems.roles
]