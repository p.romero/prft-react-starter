import { Theme } from "@material-ui/core/styles";
import { fade } from '@material-ui/core/styles/colorManipulator';
import { Z_BLOCK } from "zlib";

const style: any = (theme: Theme) => {

    const boxShadowColor = '#000';
    const transition = 'background-color 0.3s cubic-bezier(0.2,0,0,1),color 0.3s cubic-bezier(0.2,0,0,1)'
    const drawerWidth = 240;
    const firstShadow = [0, 1, 3, 0, fade(boxShadowColor, 0.12)];
    const secondShadow = [0, 1, 2, 0, fade(boxShadowColor, 0.24)];

    console.log(theme.palette.primary)
    return {
        root: {
            flexGrow: 1
        },
        container: {
            marginLeft: 40
        },
        linkGroup: {
            width: '100%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            flexDirection: 'column',
        },
        tooltip: {
            fontSize: 16,
            backgroundColor: theme.palette.secondary.dark
        },
        link: {
            padding: 4
        },
        toolbar: {
            width: 64,
            height: '100vh',
            top: 0,
            left: 0,
            display: 'flex',
            alignItems: 'flex-start',
            position: 'fixed',
            boxSizing: 'border-box',
            flexDirection: 'column',
            flexShrink: 0,
            whiteSpace: 'nowrap',
            backgroundColor: theme.palette.primary.main,
            color: 'white',
            boxShadow: [firstShadow, secondShadow],
            paddingBottom: 20,
            paddingTop: 24,
        },
        menuButton: {
            marginLeft: -12,
            marginRight: 20,
            color: "white"
        },
        hide: {
            display: 'none'
        },
        toggleButton: {
            zIndex: 100,
            position: 'absolute',
            backgroundColor: theme.palette.primary.light,
            right: -30,
            borderRadius: 5,
            color: theme.palette.primary.main,
            boxShadow: [firstShadow, secondShadow],
            top: 10
        },
        drawerOpen: {
            backgroundColor: theme.palette.primary.main,
            width: drawerWidth,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.enteringScreen,
            }),
        },
        drawerClose: {
            backgroundColor: theme.palette.primary.main,
            transition: theme.transitions.create('width', {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }),
        },
        listItem: {
            backgroundColor: 'white'
        },
        logo: {
            paddingLeft: 8,
            paddingBottom: 8,
        }
    }
}

export default style