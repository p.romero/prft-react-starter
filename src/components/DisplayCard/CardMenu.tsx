import React from 'react'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import Delete from '@material-ui/icons/Delete';


interface Props {
    id: string,
    open: boolean,
    handleClose: any,
    anchorEl: HTMLElement | null
};

function CardMenu(props: Props) {

    const { open, handleClose, anchorEl } = props;

    const handleDelete = () => { };

    return (
        <Menu open={open} onClose={handleClose} anchorEl={anchorEl}>
            <MenuItem>
                <IconButton onClick={handleDelete}><Delete /></IconButton>
                Delete
            </MenuItem>
        </Menu>
    )
}

export default CardMenu
