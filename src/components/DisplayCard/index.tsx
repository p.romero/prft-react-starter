import React, { useState } from 'react'
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import CardMenu from './CardMenu';
import MoreHoriz from '@material-ui/icons/MoreHoriz'
import { withStyles } from '@material-ui/core';
import classNames from 'classnames'
import styles from './styles';

interface Props {
    data: any,
    spacing: number, 
    key: string | number,
    classes: any,
};

function DisplayCard (props: Props) {

    const { classes, data, spacing } = props;

    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

    const {
        projectId,
        projectName,
        clientName,
        businessUnit,
        comments,
        projectStatus,
        accountDeveloper,
        projectType,
        startDate,
        endDate,
    } = data;

    const dateFormatter = (date: string) => {
        return new Date(date).toDateString().split(' ').slice(1).join(' ');
    };

    const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => setAnchorEl(e.currentTarget);
    const handleClose = () => setAnchorEl(null);

    const open = Boolean(anchorEl);

    return (
        <Grid>
            <Card className={classNames(classes.card, [projectStatus])}>
                <Grid
                    container
                    justify="space-between"
                    alignItems="flex-start"
                >
                    <Grid item xs className={classes.header}>
                        <Typography variant="h5">{projectName || 'Project Title'}</Typography>
                        <Typography variant="h6">{clientName || 'Client'}</Typography>
                        <Typography variant="caption">{dateFormatter(startDate) || 'start'} - {dateFormatter(endDate) || 'end'}</Typography>
                    </Grid>
                    <Grid item xs className={classes.options}>
                        <IconButton onClick={handleClick}>
                            <MoreHoriz />
                        </IconButton>
                        <CardMenu id="actionsMenu" open={open} anchorEl={anchorEl} handleClose={handleClose} />
                    </Grid>
                </Grid>
                <Typography variant="subtitle1">{accountDeveloper || 'Account Developer'}</Typography>
                <Typography variant='subtitle2'>{businessUnit || 'Business Unit'}</Typography>
                <Typography className={classes.type} variant='subtitle2' gutterBottom>{projectType || 'Project Type'}</Typography>
                <Typography className={classes.comments} paragraph>{comments || 'Project Comments'}</Typography>
            </Card >
        </Grid >

    )
}

export default withStyles(styles)(DisplayCard)
