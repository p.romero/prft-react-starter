import { Theme } from "@material-ui/core";
import { fade } from '@material-ui/core/styles/colorManipulator';

const style: any = (theme: Theme) => {

    const boxShadowColor = '#000'
    const firstShadow = [0, 1, 3, 0, fade(boxShadowColor, 0.12)];
    const secondShadow = [0, 1, 2, 0, fade(boxShadowColor, 0.24)];

    return {
        card: {
            height: 250,
            width: 250,
            padding: 24,
            margin: 24,
            borderRadius: '5px',
            boxShadow: [firstShadow, secondShadow],
            transform: 'perspective(1px) translateZ(0)',
            transitionDuration: '0.3s',
            transitionProperty: 'transform',
            '&:hover': {
                cursor: 'pointer',
                transform: 'scale(1.1)',
                boxShadow: [firstShadow, secondShadow],
            }
        },
        header: {
            flexGrow: 1,
            marginBottom: 24
        },
        cover: {
            width: 24
        },
        verbal: {
            borderLeft: '12px solid purple'
        },
        proposed: {
            borderLeft: '12px solid yellow'
        },
        sold: {
            borderLeft: '12px solid green'
        },
        archived: {
            borderLeft: '12px solid gray'
        },
        type: {
            color: theme.palette.primary.dark
        },
        comments: {
            color: theme.palette.secondary
        },
        options: {
            display: 'flex',
            flexDirection: 'row-reverse'
        }
    }
}

export default style