import React from 'react'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider';
import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles'

interface Props {
    classes: any;
    page?: string;
    title?: string;
}

function Header(props: Props) {

    const { classes, page, title } = props;

    return (

        <div className={classes.header}>
            <Typography color='secondary' variant='overline'>
                {page || 'page'}
            </Typography>
            <Typography color='secondary' variant='h5'>
                {title || 'title'}
            </Typography>
            <Divider />
        </div>

    )
};


export default withStyles(styles)(Header)
