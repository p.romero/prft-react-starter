export const styles = () => ({
    header: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column' as 'column',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        padding: 16,
        margin: 8,
    }
})