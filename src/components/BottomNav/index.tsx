import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import {BottomNavItems} from './BottomNavItems'
import { Icon } from '@material-ui/core';

interface Props {
    navItem: any,
    classes: any
};

function BottomNav(props: Props) {
    
    const { navItem, classes } = props;

    return (
        <BottomNavigation className={classes.root}>
            {BottomNavItems.map((item, i) => { 
                <BottomNavigationAction label={item.name} value={item.name}  />
            })}
        </BottomNavigation>
    );
}

export default BottomNav
