import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Projects from "./pages/Projects";
import Layout from "./layout/Layout";
import Roles from "./pages/Roles";
import Consultants from "./pages/Consultants";

const Router = () => {
  return (
      <Layout>
        <BrowserRouter>
          <div className="view">
            <Switch>
              <Route exact path="/" component={Projects}/>
              <Route exact path="/roles" component={Roles}/>
              <Route exact path="/consultants" component={Consultants}/>
            </Switch>
          </div>
        </BrowserRouter>
      </Layout>
  );
};

export default Router;
