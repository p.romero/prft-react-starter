import React, { useState, useEffect } from "react";

import { withStyles, Theme } from "@material-ui/core";
import CardGrid from "../containers/CardGrid";
import DisplayCard from '../components/DisplayCard';
import * as _ from 'lodash';
import getProjects from "../api/projects/project/getProjects";

interface IProps {
  filterDataObject: IfilterData;
  classes: Theme;
}

interface IfilterData {
  projectStatusId: string,
  projectTypeId: string,
  clientId: string,
  businessUnitId: string
}

interface ProjectData {
  projectId: string | number
  projectName: string
  companyName: string
  businessUnit: string
  projectStatus: string
  projectStatusId: string
  accountDeveloper: string
  projectType: string
  startDate: string
  endDate: string
}

function Projects(props: IProps) {

  const [filter, setFilter] = useState(false);
  const [filterData, setFilterData] = useState<IfilterData | null>(null);
  const [sortByField, setSortByField] = useState('client.name')
  const [projectData, setData] = useState<ProjectData | null>({
    projectId: '',
    projectName: '',
    companyName: '',
    businessUnit: '',
    projectStatus: '',
    projectStatusId: '',
    accountDeveloper: '',
    projectType: '',
    startDate: '',
    endDate: ''
  });
  const [size, setSize] = useState(20);
  const [variant, setVariant] = useState('outlined');
  const [open, setOpen] = useState(false);
  const [name, setName] = useState('');

  useEffect(() => {
    // getAllProjects();
    console.log('PROJECT DATA', projectData)        // setData(projects)
  }, [projectData])

  const getAllProjects = async () => {
    const data = await getProjects(name, size);
    setData(data.data.content)
  };

  const toggleOpen = () => {
    setOpen(!open)
  };

  const getRoles = (projectId: string) => {

  };

  const getFilters = (filterData: IfilterData) => {
    setFilterData(filterData);
    setFilter(true);
  };

  const handleClick = () => {
    setSize(size + 20);
  }

  const handleFilter = () => {
  };

  const changeSortBy = (sortByField: string) => {
    setSortByField(sortByField);
    setVariant('contained')
  };

  const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
    e && setName(e.currentTarget.value)
  };

  return (
    <>
      <CardGrid title="projects">
        {projectData && _.map(projectData, () => {
          return (
            <DisplayCard
              spacing={8}
              key={projectData.projectId}
              data={projectData}
            />);
        })}
      </CardGrid>
    </>
  )
};

export default Projects