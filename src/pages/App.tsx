import React, { Component } from "react";

import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import theme from "../styles/theme/theme";

import "../styles/App.css";

import Router from "../router";

class App extends Component {
  render() {
    return (
        <MuiThemeProvider theme={theme}>
          <div className="App">
            <Router/>
          </div>
        </MuiThemeProvider>
    );
  }
}

export default App;
