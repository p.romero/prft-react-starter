import axios from "axios";

import urls from "../../routes/routes";

async function addOrUpdateProject(data : any, add : boolean) {
  const reqBody = {
    // TODO add request body data to send to project API
    // Example: 
    // id : data.id
  };

  // if new project, send POST request, otherwise send PUT request to update.
  if ( add ) {
    return await axios.post(`${urls.projects}`, reqBody);
  } else {
    return await axios.put(`${urls.projects}`, reqBody);
  }
}

export default addOrUpdateProject;
