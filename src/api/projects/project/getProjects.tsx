import axios from "axios";

import urls from "../../routes/routes";

const getProjects = async (name : String, size : number) => {
  return await axios.get(`${urls.projects}?name=${name}&size=${size}`);
};

export default getProjects;
